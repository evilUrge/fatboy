FROM python:3.7.0


ADD . /app
WORKDIR /app

RUN pip install -r /app/requirements.txt
ENV PYTHONPATH $pwd
ENV DOCKER 1

CMD ["python", "run.py"]

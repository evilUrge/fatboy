# Fatman! - Load test for CI
_Okay it's also human friendly_

**Why Fatman?** - Because you can stress test everything **with a JWT auth support**!


## Prerequisites
Python 3.6.5


Install requirements by:
```
pip3 install -r requirements.txt
````
**It's recommended to create a virtualenv before**

## Optional arguments:
  * **-h, --help** show this help message and exit

  * **-u URL, --url URL** URL to bomb!

  * **-t Time to bomb, --time Time to bomb** For how long should I run?

  * **-w Amount of workers, --workers Amount of workers** Amount of workers (default 3)

  * **-d Delay time between calls, --delay Delay time between calls** Wait between calls in milliseconds (default 100)

  * **-m Method to use, --method Method to use** GET POST or PUT (changing to post when using auth)

  * **-c Expected status code, --code Expected status code** Expected status code (default 200)

  * **-H Expected header, --header Expected header** Expected header response from the server(JSON obj)

  * **-b Expected body, --body Expected body** Expected body response from the server(JSON obj)

  * **-j JWT, --jwt JWT** Authentication with JWT(JSON obj)

  * **-v, --version** show program's version number and exit
----------------------------------------------------------


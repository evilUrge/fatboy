import argparse
import json
import logging
from concurrent.futures import ThreadPoolExecutor, as_completed
from os import environ
from time import sleep, time

import requests
from requests_jwt import JWTAuth


def external_request(method, url, headers=False, body=False, jwt=False):
    try:
        if method == 'POST':
            r = requests.post(url=url, data=body if body else {}, headers=headers,
                              auth=JWTAuth(jwt) if jwt else None, timeout=None)

        elif method == 'PUT':
            r = requests.post(url=url, data=body if body else {}, headers=headers,
                              auth=JWTAuth(jwt) if jwt else None, timeout=None)

        elif method == 'PATCH':
            r = requests.patch(url=url, data=body if body else {}, headers=headers,
                               auth=JWTAuth(jwt) if jwt else None, timeout=None)
        else:
            r = requests.get(url=url, timeout=None)

        return {'url': r.url, 'code': r.status_code, 'body': r.json(), 'header': r.headers}
    except ConnectionError as e:
        return {}


class FatMan:
    def __init__(self, url, exec_time, delay, workers, method, code, body, header, jwt):
        def init_logger():
            logger = logging
            logger.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
            return logger

        self.url = 'http://{}'.format(url) if not url.startswith('http://') or url.startswith('https://') else url
        self.method = method.upper() if method else 'GET'
        self.exec_time = int(exec_time)
        self.delay = float(delay) / 1000 if delay else 100
        self.workers = int(workers) if workers else 3
        self.expected_code = int(code) if code else 200
        self.expected_body = json.loads(body) if body else None
        self.expected_header = json.loads(header) if header else None
        self.jwt = open(jwt) if jwt else None
        self.log = init_logger()
        self.calls = []

    def __result_validator__(self):
        result = []

        for call in as_completed(self.calls):
            if call:
                call = call.result()
                result.append(
                    self.expected_code == call.get('code')  # TODO: Add AND body AND head
                )

        return all(result)

    def execute(self):
        def stats(calls):
            failed = calls.count(False)
            all_calls = len(calls)
            passed = all_calls - failed
            return {
                'failed': failed,
                'all': all_calls,
                'passed': passed}

        end_time = time() + self.exec_time

        with ThreadPoolExecutor(max_workers=self.workers) as executor:
            while time() < end_time:
                self.calls.append(executor.submit(external_request, self.method, self.url,self.expected_header, body=self.expected_body, jwt=self.jwt))
                sleep(self.delay)

        if self.__result_validator__():
            self.log.info("Your microservice passed stress!")
        else:
            self.log.error("Your microservice failed stresstest!")

        run_stats = stats(self.calls)  # Todo: Make this more slim

        self.log.info("Made {} calls in {} seconds".format(run_stats['all'], self.exec_time))
        self.log.info('{} out of {}'.format(run_stats['passed'], run_stats['all']))


if __name__ == '__main__':
    if environ.get('PYDEBUGXL'):  # Add the env param in PyCharm for debugging
        FatMan(url='http://10.51.62.64:28307/v1/inventory/plants/1000/skus/3333', exec_time=60, delay=5, workers=200,
               header=False, body='{"amount": 10,"last_updated": "2018-05-05 12:12:12"}',
               method='PATCH', code=False, jwt=False).execute()

    elif environ.get('DOCKER'):
        FatMan(url=environ.get('url'), exec_time=environ.get('time'), delay=environ.get('delay'),
               workers=environ.get('workers'), header=environ.get('headers'), body=environ.get('body'),
               method=environ.get('method'), code=environ.get('code'), jwt=environ.get('jwt')).execute()

    else:
        parser = argparse.ArgumentParser(description='FatManXL! - Make a mess')
        parser.add_argument('-u', '--url', metavar='URL',
                            help='URL to bomb!', required=True)
        parser.add_argument('-t', '--time', metavar='Time to bomb',
                            help='Fow how long should I run?', required=True)
        parser.add_argument('-w', '--workers', metavar='Amount of workers',
                            help="Amount of workers (default 3)", required=False)
        parser.add_argument('-d', '--delay', metavar='Delay time between calls',
                            help="Wait between calls in milliseconds (default 100)", required=False)
        parser.add_argument('-m', '--method', metavar='Method to use',
                            help='GET POST or PUT (changing to post when using auth)', required=False)
        parser.add_argument('-c', '--code', metavar='Expected status code',
                            help='Expected status code (default 200)', required=False)
        parser.add_argument('-H', '--header', metavar='Expected header',
                            help='Expected header response from the server(JSON obj)', required=False)
        parser.add_argument('-b', '--body', metavar='Expected body',
                            help='Expected body response from the server(JSON obj)', required=False)
        parser.add_argument('-j', '--jwt', metavar='JWT',
                            help='Authentication with JWT(JSON obj)', required=False)

        parser.add_argument('-v', '--version', action='version', version='FatBoyXL 0.1')

        args = vars(parser.parse_args())
        FatMan(url=args.get('url'), exec_time=args.get('time'), workers=args.get('workers'), delay=args.get('delay'),
               method=args.get('method'), code=args.get('code'), body=args.get('body'),
               jwt=args.get('jwt'), header=args.get('header')).execute()
